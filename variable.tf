variable "instance_ami" {
    default = "ami-0fb653ca2d3203ac1"
}

variable "instance_type" {
    default = "t2.micro"
}

variable "cidr_block" {
    default = "10.0.0.0/22"
}

variable "vpc_name" {
    default = "ravi_vpc"
}
